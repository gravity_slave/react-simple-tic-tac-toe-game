import React, { Component } from 'react';
import './App.css';
import Square from "./components/Square";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
          currentPlayer: 'fp',
            winner: '',
        };
    }
    changeCurrentPlayer = (e) => {
        if (this.state.currentPlayer === 'fp') {
            this.setState({
                currentPlayer: 'sp'
            }, this.checkIfWin);
        } else if (this.state.currentPlayer === 'sp') {
            this.setState({
                currentPlayer: 'fp',
            },
                this.checkIfWin
            );
        }

    };

    checkIfWin =() => {
      const elems =  Array.prototype.slice.call(document.getElementsByClassName('element'));
      const allSymVal = elems.map((el) => {
          return el.innerHTML;
      });
      let {winner} = this.state;
       this.state.currentPlayer === 'sp' ?
           this.setState({
               winner: "First Player",
           })
           : this.setState({
           winner: 'First Player',
       });


      const comboForWin = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
      comboForWin.find((combo) => {
          if (  allSymVal[combo[0]] !== "" &&  allSymVal[combo[0]] === allSymVal[combo[1]] && allSymVal[combo[1]] === allSymVal[combo[2]] ) {
              alert(`The Winner is player ${winner}`);
              this.resetAll();
              return allSymVal[combo[0]];

          } else {

              return false;
          }
      });
    };

    resetAll =() => {
        this.setState({
            currentPlayer: 'fp',
            winner: ''
        });
   const resetElems = Array.prototype.slice.call(document.getElementsByClassName('element'));
   resetElems.forEach((curEl) => {
      return curEl.innerHTML = "";
   });
    };
  render() {

    return (
      <div className="App">
          <Square currentPlayer={this.state.currentPlayer} changePlayer={this.changeCurrentPlayer} />
          <Square currentPlayer={this.state.currentPlayer} changePlayer={this.changeCurrentPlayer} />
          <Square currentPlayer={this.state.currentPlayer} changePlayer={this.changeCurrentPlayer} />
          <Square currentPlayer={this.state.currentPlayer} changePlayer={this.changeCurrentPlayer} />
          <Square currentPlayer={this.state.currentPlayer} changePlayer={this.changeCurrentPlayer} />
          <Square currentPlayer={this.state.currentPlayer} changePlayer={this.changeCurrentPlayer} />
          <Square currentPlayer={this.state.currentPlayer} changePlayer={this.changeCurrentPlayer} />
          <Square currentPlayer={this.state.currentPlayer} changePlayer={this.changeCurrentPlayer} />
          <Square currentPlayer={this.state.currentPlayer} changePlayer={this.changeCurrentPlayer} />
          <div className="btn" onClick={this.resetAll}>
              <button> Reset All</button>
          </div>
      </div>
    );
  }
}

export default App;
