import React, {Component} from 'react';

class Square extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isClicked: false
        }
    }

    handleClick = (e) => {
        const x_sym = 'X';
        const o_sym = 'O';

        if (this.state.isClicked === false || ( this.state.isClicked === true && e.target.innerHTML === "")) {
            let {currentPlayer} = this.props;
            if (currentPlayer === 'fp') {
                e.target.innerHTML = x_sym;
                this.props.changePlayer();

            } else if(currentPlayer ==='sp') {
                e.target.innerHTML = o_sym;
                this.props.changePlayer();
            }
            console.log('is clickeded ' + this.state.isClicked);

            this.setState({
                isClicked: true
            });
            console.log('is clickeded ' + this.state.isClicked);
        } else {
            return false;
        }

    };
    render() {



        return (
            <div className="element" onClick={this.handleClick}>

            </div>
        );
    }
}



export default Square;
